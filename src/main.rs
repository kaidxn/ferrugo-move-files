use std::fs::{create_dir, read_dir, rename, DirEntry};
use std::io;
use std::path::{Path, PathBuf};

struct UserSettings {
    source_folder: PathBuf,
    dest_folder: PathBuf,
    identification_text: String,
    is_prefix: bool,
    is_suffix: bool,
    id_list: Vec<String>,
}

fn get_user_input() -> String {
    let mut input: String = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read input");
    if input.trim().len() == 0 {
        input.push('.');
    }
    input
}

fn create_path(path: String) -> PathBuf {
    let files: &Path = Path::new(path.trim());
    return files.to_owned();
}

fn move_file(file: DirEntry, folder: &Path) -> Result<DirEntry, io::Error> {
    let folder = folder.join(file.file_name());
    rename(file.path(), folder)?;
    return Ok(file);
}

fn check_folder_exists(folder: &str) -> Result<(), io::Error> {
    let folder_object = Path::new(folder);
    if !folder_object.exists() {
        return Err(io::Error::new(io::ErrorKind::NotFound, "Folder not found"));
    }
    Ok(())
}

fn ask_for_folder() -> PathBuf {
    loop {
        let file_path = create_path(get_user_input());
        if check_folder_exists(&file_path.as_path().to_str().unwrap()).is_err() {
            println!("Invalid folder");
            continue;
        }
        return file_path;
    }
}

fn ask_for_configuration() -> UserSettings {
    println!("Insert source path: ");
    let source_pathbuf: PathBuf = ask_for_folder();
    println!("Insert name of the destination folder: ");
    let mut dest_pathbuf = source_pathbuf.clone();
    dest_pathbuf.push(get_user_input().trim_end());
    let mut is_suffix = false;
    let mut is_prefix = false;
    println!("Insert the identification text: ");
    let identification_text = get_user_input().trim_end().to_owned();
    loop {
        println!("Is it a prefix or a suffix?\n 1. Prefix \n 2. Suffix");
        let prefix_or_suffix = get_user_input();
        match prefix_or_suffix.trim().parse() {
            Ok(num) => {
                let choice: i32 = num;
                if choice == 1 {
                    is_prefix = true;
                    break;
                } else if choice == 2 {
                    is_suffix = true;
                    break;
                }
                println!("Invalid choice. Please choose between 1 or 2");
            }
            Err(_) => println!("Invalid input. Please insert a number"),
        }
    }
    let mut id_list: Vec<String> = Vec::new();
    loop {
        println!("Insert the list of IDs separated by semi-collon(;):");
        get_user_input()
            .split(';')
            .for_each(|id| id_list.push(id.to_string()));
        if id_list.len() > 1 {
            break;
        }
        println!("Please insert at least one ID.");
    }
    UserSettings {
        source_folder: source_pathbuf,
        dest_folder: dest_pathbuf,
        identification_text,
        is_prefix,
        is_suffix,
        id_list,
    }
}

fn main() {
    let user_settings = ask_for_configuration();
    let file_list: Vec<_> = read_dir(&user_settings.source_folder)
        .unwrap()
        .filter_map(|file| file.ok())
        .filter(|file| file.file_type().unwrap().is_file())
        .collect();
    create_dir(user_settings.dest_folder.as_path()).expect("Não foi possivel criar a pasta");
    for file in file_list {
        let moved_file: Result<DirEntry, io::Error> =
            move_file(file, &user_settings.dest_folder.as_path());
        println!(
            "{} moved",
            moved_file.unwrap().file_name().to_str().unwrap()
        );
    }
}
